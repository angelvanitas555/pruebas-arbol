/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "reservas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservas.findAll", query = "SELECT r FROM Reservas r")
    , @NamedQuery(name = "Reservas.findByIdReserva", query = "SELECT r FROM Reservas r WHERE r.idReserva = :idReserva")
    , @NamedQuery(name = "Reservas.findByNombreOtro", query = "SELECT r FROM Reservas r WHERE r.nombreOtro = :nombreOtro")
    , @NamedQuery(name = "Reservas.findByHoraFecha", query = "SELECT r FROM Reservas r WHERE r.horaFecha = :horaFecha")
    , @NamedQuery(name = "Reservas.findByCantidadComensales", query = "SELECT r FROM Reservas r WHERE r.cantidadComensales = :cantidadComensales")
    , @NamedQuery(name = "Reservas.findByNumeroContacto", query = "SELECT r FROM Reservas r WHERE r.numeroContacto = :numeroContacto")
    , @NamedQuery(name = "Reservas.findByEmail", query = "SELECT r FROM Reservas r WHERE r.email = :email")})
public class Reservas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_reserva")
    private Integer idReserva;
    @Column(name = "nombre_otro")
    private String nombreOtro;
    @Column(name = "hora_fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaFecha;
    @Column(name = "cantidad_comensales")
    private Integer cantidadComensales;
    @Column(name = "numero_contacto")
    private String numeroContacto;
    @Column(name = "email")
    private String email;
    @JoinColumn(name = "area_restaurante", referencedColumnName = "id_area")
    @ManyToOne
    private AreasRestaurante areaRestaurante;
    @JoinColumn(name = "restaurante", referencedColumnName = "id_restaurante")
    @ManyToOne
    private Restaurantes restaurante;

    public Reservas() {
    }

    public Reservas(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public Integer getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Integer idReserva) {
        this.idReserva = idReserva;
    }

    public String getNombreOtro() {
        return nombreOtro;
    }

    public void setNombreOtro(String nombreOtro) {
        this.nombreOtro = nombreOtro;
    }

    public Date getHoraFecha() {
        return horaFecha;
    }

    public void setHoraFecha(Date horaFecha) {
        this.horaFecha = horaFecha;
    }

    public Integer getCantidadComensales() {
        return cantidadComensales;
    }

    public void setCantidadComensales(Integer cantidadComensales) {
        this.cantidadComensales = cantidadComensales;
    }

    public String getNumeroContacto() {
        return numeroContacto;
    }

    public void setNumeroContacto(String numeroContacto) {
        this.numeroContacto = numeroContacto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AreasRestaurante getAreaRestaurante() {
        return areaRestaurante;
    }

    public void setAreaRestaurante(AreasRestaurante areaRestaurante) {
        this.areaRestaurante = areaRestaurante;
    }

    public Restaurantes getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurantes restaurante) {
        this.restaurante = restaurante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReserva != null ? idReserva.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservas)) {
            return false;
        }
        Reservas other = (Reservas) object;
        if ((this.idReserva == null && other.idReserva != null) || (this.idReserva != null && !this.idReserva.equals(other.idReserva))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.Reservas[ idReserva=" + idReserva + " ]";
    }
    
}
