/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "admin_master")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminMaster.findAll", query = "SELECT a FROM AdminMaster a")
    , @NamedQuery(name = "AdminMaster.findByIdMaster", query = "SELECT a FROM AdminMaster a WHERE a.idMaster = :idMaster")
    , @NamedQuery(name = "AdminMaster.findByNombre", query = "SELECT a FROM AdminMaster a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "AdminMaster.findByPassword", query = "SELECT a FROM AdminMaster a WHERE a.password = :password")})
public class AdminMaster implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_master")
    private Integer idMaster;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "password")
    private String password;

    public AdminMaster() {
    }

    public AdminMaster(Integer idMaster) {
        this.idMaster = idMaster;
    }

    public Integer getIdMaster() {
        return idMaster;
    }

    public void setIdMaster(Integer idMaster) {
        this.idMaster = idMaster;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMaster != null ? idMaster.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminMaster)) {
            return false;
        }
        AdminMaster other = (AdminMaster) object;
        if ((this.idMaster == null && other.idMaster != null) || (this.idMaster != null && !this.idMaster.equals(other.idMaster))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.AdminMaster[ idMaster=" + idMaster + " ]";
    }
    
}
