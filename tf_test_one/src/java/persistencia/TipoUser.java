/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "tipo_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoUser.findAll", query = "SELECT t FROM TipoUser t")
    , @NamedQuery(name = "TipoUser.findByIdTipoUser", query = "SELECT t FROM TipoUser t WHERE t.idTipoUser = :idTipoUser")
    , @NamedQuery(name = "TipoUser.findByTipoUser", query = "SELECT t FROM TipoUser t WHERE t.tipoUser = :tipoUser")})
public class TipoUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_TIPO_USER")
    private Integer idTipoUser;
    @Column(name = "tipo_user")
    private String tipoUser;

    public TipoUser() {
    }

    public TipoUser(Integer idTipoUser) {
        this.idTipoUser = idTipoUser;
    }

    public Integer getIdTipoUser() {
        return idTipoUser;
    }

    public void setIdTipoUser(Integer idTipoUser) {
        this.idTipoUser = idTipoUser;
    }

    public String getTipoUser() {
        return tipoUser;
    }

    public void setTipoUser(String tipoUser) {
        this.tipoUser = tipoUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoUser != null ? idTipoUser.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoUser)) {
            return false;
        }
        TipoUser other = (TipoUser) object;
        if ((this.idTipoUser == null && other.idTipoUser != null) || (this.idTipoUser != null && !this.idTipoUser.equals(other.idTipoUser))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.TipoUser[ idTipoUser=" + idTipoUser + " ]";
    }
    
}
