/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "areas_restaurante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AreasRestaurante.findAll", query = "SELECT a FROM AreasRestaurante a")
    , @NamedQuery(name = "AreasRestaurante.findByIdArea", query = "SELECT a FROM AreasRestaurante a WHERE a.idArea = :idArea")
    , @NamedQuery(name = "AreasRestaurante.findByArea", query = "SELECT a FROM AreasRestaurante a WHERE a.area = :area")})
public class AreasRestaurante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area")
    private Integer idArea;
    @Column(name = "area")
    private String area;
    @OneToMany(mappedBy = "areaRestaurante")
    private List<Reservas> reservasList;

    public AreasRestaurante() {
    }

    public AreasRestaurante(Integer idArea) {
        this.idArea = idArea;
    }

    public Integer getIdArea() {
        return idArea;
    }

    public void setIdArea(Integer idArea) {
        this.idArea = idArea;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @XmlTransient
    public List<Reservas> getReservasList() {
        return reservasList;
    }

    public void setReservasList(List<Reservas> reservasList) {
        this.reservasList = reservasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArea != null ? idArea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AreasRestaurante)) {
            return false;
        }
        AreasRestaurante other = (AreasRestaurante) object;
        if ((this.idArea == null && other.idArea != null) || (this.idArea != null && !this.idArea.equals(other.idArea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.AreasRestaurante[ idArea=" + idArea + " ]";
    }
    
}
