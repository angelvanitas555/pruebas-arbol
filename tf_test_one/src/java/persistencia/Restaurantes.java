/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jose.azucenaUSAM
 */
@Entity
@Table(name = "restaurantes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Restaurantes.findAll", query = "SELECT r FROM Restaurantes r")
    , @NamedQuery(name = "Restaurantes.findByIdRestaurante", query = "SELECT r FROM Restaurantes r WHERE r.idRestaurante = :idRestaurante")
    , @NamedQuery(name = "Restaurantes.findByNombre", query = "SELECT r FROM Restaurantes r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "Restaurantes.findByEmail", query = "SELECT r FROM Restaurantes r WHERE r.email = :email")})
public class Restaurantes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_restaurante")
    private Integer idRestaurante;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "email")
    private String email;
    @OneToMany(mappedBy = "idRestaurante")
    private List<SucuralRestaurante> sucuralRestauranteList;
    @OneToMany(mappedBy = "restaurante")
    private List<Reservas> reservasList;
    @JoinColumn(name = "direccion", referencedColumnName = "id_direccion")
    @ManyToOne
    private Direcciones direccion;
    @JoinColumn(name = "telefono", referencedColumnName = "id_telefono")
    @ManyToOne
    private Telefonos telefono;

    public Restaurantes() {
    }

    public Restaurantes(Integer idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    public Integer getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(Integer idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public List<SucuralRestaurante> getSucuralRestauranteList() {
        return sucuralRestauranteList;
    }

    public void setSucuralRestauranteList(List<SucuralRestaurante> sucuralRestauranteList) {
        this.sucuralRestauranteList = sucuralRestauranteList;
    }

    @XmlTransient
    public List<Reservas> getReservasList() {
        return reservasList;
    }

    public void setReservasList(List<Reservas> reservasList) {
        this.reservasList = reservasList;
    }

    public Direcciones getDireccion() {
        return direccion;
    }

    public void setDireccion(Direcciones direccion) {
        this.direccion = direccion;
    }

    public Telefonos getTelefono() {
        return telefono;
    }

    public void setTelefono(Telefonos telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRestaurante != null ? idRestaurante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Restaurantes)) {
            return false;
        }
        Restaurantes other = (Restaurantes) object;
        if ((this.idRestaurante == null && other.idRestaurante != null) || (this.idRestaurante != null && !this.idRestaurante.equals(other.idRestaurante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.Restaurantes[ idRestaurante=" + idRestaurante + " ]";
    }
    
}
