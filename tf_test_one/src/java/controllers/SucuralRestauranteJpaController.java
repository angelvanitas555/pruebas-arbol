/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Restaurantes;
import persistencia.SucuralRestaurante;
import persistencia.Sucursales;

/**
 *
 * @author Angel
 */
public class SucuralRestauranteJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public SucuralRestauranteJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tf_test_onePU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    


    public void create(SucuralRestaurante sucuralRestaurante) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Restaurantes idRestaurante = sucuralRestaurante.getIdRestaurante();
            if (idRestaurante != null) {
                idRestaurante = em.getReference(idRestaurante.getClass(), idRestaurante.getIdRestaurante());
                sucuralRestaurante.setIdRestaurante(idRestaurante);
            }
            Sucursales idSucursal = sucuralRestaurante.getIdSucursal();
            if (idSucursal != null) {
                idSucursal = em.getReference(idSucursal.getClass(), idSucursal.getIdSucursal());
                sucuralRestaurante.setIdSucursal(idSucursal);
            }
            em.persist(sucuralRestaurante);
            if (idRestaurante != null) {
                idRestaurante.getSucuralRestauranteList().add(sucuralRestaurante);
                idRestaurante = em.merge(idRestaurante);
            }
            if (idSucursal != null) {
                idSucursal.getSucuralRestauranteList().add(sucuralRestaurante);
                idSucursal = em.merge(idSucursal);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SucuralRestaurante sucuralRestaurante) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SucuralRestaurante persistentSucuralRestaurante = em.find(SucuralRestaurante.class, sucuralRestaurante.getIdPivoSucursalRestaurante());
            Restaurantes idRestauranteOld = persistentSucuralRestaurante.getIdRestaurante();
            Restaurantes idRestauranteNew = sucuralRestaurante.getIdRestaurante();
            Sucursales idSucursalOld = persistentSucuralRestaurante.getIdSucursal();
            Sucursales idSucursalNew = sucuralRestaurante.getIdSucursal();
            if (idRestauranteNew != null) {
                idRestauranteNew = em.getReference(idRestauranteNew.getClass(), idRestauranteNew.getIdRestaurante());
                sucuralRestaurante.setIdRestaurante(idRestauranteNew);
            }
            if (idSucursalNew != null) {
                idSucursalNew = em.getReference(idSucursalNew.getClass(), idSucursalNew.getIdSucursal());
                sucuralRestaurante.setIdSucursal(idSucursalNew);
            }
            sucuralRestaurante = em.merge(sucuralRestaurante);
            if (idRestauranteOld != null && !idRestauranteOld.equals(idRestauranteNew)) {
                idRestauranteOld.getSucuralRestauranteList().remove(sucuralRestaurante);
                idRestauranteOld = em.merge(idRestauranteOld);
            }
            if (idRestauranteNew != null && !idRestauranteNew.equals(idRestauranteOld)) {
                idRestauranteNew.getSucuralRestauranteList().add(sucuralRestaurante);
                idRestauranteNew = em.merge(idRestauranteNew);
            }
            if (idSucursalOld != null && !idSucursalOld.equals(idSucursalNew)) {
                idSucursalOld.getSucuralRestauranteList().remove(sucuralRestaurante);
                idSucursalOld = em.merge(idSucursalOld);
            }
            if (idSucursalNew != null && !idSucursalNew.equals(idSucursalOld)) {
                idSucursalNew.getSucuralRestauranteList().add(sucuralRestaurante);
                idSucursalNew = em.merge(idSucursalNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sucuralRestaurante.getIdPivoSucursalRestaurante();
                if (findSucuralRestaurante(id) == null) {
                    throw new NonexistentEntityException("The sucuralRestaurante with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SucuralRestaurante sucuralRestaurante;
            try {
                sucuralRestaurante = em.getReference(SucuralRestaurante.class, id);
                sucuralRestaurante.getIdPivoSucursalRestaurante();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sucuralRestaurante with id " + id + " no longer exists.", enfe);
            }
            Restaurantes idRestaurante = sucuralRestaurante.getIdRestaurante();
            if (idRestaurante != null) {
                idRestaurante.getSucuralRestauranteList().remove(sucuralRestaurante);
                idRestaurante = em.merge(idRestaurante);
            }
            Sucursales idSucursal = sucuralRestaurante.getIdSucursal();
            if (idSucursal != null) {
                idSucursal.getSucuralRestauranteList().remove(sucuralRestaurante);
                idSucursal = em.merge(idSucursal);
            }
            em.remove(sucuralRestaurante);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SucuralRestaurante> findSucuralRestauranteEntities() {
        return findSucuralRestauranteEntities(true, -1, -1);
    }

    public List<SucuralRestaurante> findSucuralRestauranteEntities(int maxResults, int firstResult) {
        return findSucuralRestauranteEntities(false, maxResults, firstResult);
    }

    private List<SucuralRestaurante> findSucuralRestauranteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SucuralRestaurante.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SucuralRestaurante findSucuralRestaurante(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SucuralRestaurante.class, id);
        } finally {
            em.close();
        }
    }

    public int getSucuralRestauranteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SucuralRestaurante> rt = cq.from(SucuralRestaurante.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
