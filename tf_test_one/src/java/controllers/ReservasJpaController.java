/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.AreasRestaurante;
import persistencia.Reservas;
import persistencia.Usuarios;
import persistencia.Restaurantes;

/**
 *
 * @author Angel
 */
public class ReservasJpaController implements Serializable {

    public ReservasJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Reservas reservas) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AreasRestaurante areaRestaurante = reservas.getAreaRestaurante();
            if (areaRestaurante != null) {
                areaRestaurante = em.getReference(areaRestaurante.getClass(), areaRestaurante.getIdArea());
                reservas.setAreaRestaurante(areaRestaurante);
            }
            Usuarios nombrecliente = reservas.getNombrecliente();
            if (nombrecliente != null) {
                nombrecliente = em.getReference(nombrecliente.getClass(), nombrecliente.getIdUsuario());
                reservas.setNombrecliente(nombrecliente);
            }
            Restaurantes restaurante = reservas.getRestaurante();
            if (restaurante != null) {
                restaurante = em.getReference(restaurante.getClass(), restaurante.getIdRestaurante());
                reservas.setRestaurante(restaurante);
            }
            em.persist(reservas);
            if (areaRestaurante != null) {
                areaRestaurante.getReservasList().add(reservas);
                areaRestaurante = em.merge(areaRestaurante);
            }
            if (nombrecliente != null) {
                nombrecliente.getReservasList().add(reservas);
                nombrecliente = em.merge(nombrecliente);
            }
            if (restaurante != null) {
                restaurante.getReservasList().add(reservas);
                restaurante = em.merge(restaurante);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Reservas reservas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reservas persistentReservas = em.find(Reservas.class, reservas.getIdReserva());
            AreasRestaurante areaRestauranteOld = persistentReservas.getAreaRestaurante();
            AreasRestaurante areaRestauranteNew = reservas.getAreaRestaurante();
            Usuarios nombreclienteOld = persistentReservas.getNombrecliente();
            Usuarios nombreclienteNew = reservas.getNombrecliente();
            Restaurantes restauranteOld = persistentReservas.getRestaurante();
            Restaurantes restauranteNew = reservas.getRestaurante();
            if (areaRestauranteNew != null) {
                areaRestauranteNew = em.getReference(areaRestauranteNew.getClass(), areaRestauranteNew.getIdArea());
                reservas.setAreaRestaurante(areaRestauranteNew);
            }
            if (nombreclienteNew != null) {
                nombreclienteNew = em.getReference(nombreclienteNew.getClass(), nombreclienteNew.getIdUsuario());
                reservas.setNombrecliente(nombreclienteNew);
            }
            if (restauranteNew != null) {
                restauranteNew = em.getReference(restauranteNew.getClass(), restauranteNew.getIdRestaurante());
                reservas.setRestaurante(restauranteNew);
            }
            reservas = em.merge(reservas);
            if (areaRestauranteOld != null && !areaRestauranteOld.equals(areaRestauranteNew)) {
                areaRestauranteOld.getReservasList().remove(reservas);
                areaRestauranteOld = em.merge(areaRestauranteOld);
            }
            if (areaRestauranteNew != null && !areaRestauranteNew.equals(areaRestauranteOld)) {
                areaRestauranteNew.getReservasList().add(reservas);
                areaRestauranteNew = em.merge(areaRestauranteNew);
            }
            if (nombreclienteOld != null && !nombreclienteOld.equals(nombreclienteNew)) {
                nombreclienteOld.getReservasList().remove(reservas);
                nombreclienteOld = em.merge(nombreclienteOld);
            }
            if (nombreclienteNew != null && !nombreclienteNew.equals(nombreclienteOld)) {
                nombreclienteNew.getReservasList().add(reservas);
                nombreclienteNew = em.merge(nombreclienteNew);
            }
            if (restauranteOld != null && !restauranteOld.equals(restauranteNew)) {
                restauranteOld.getReservasList().remove(reservas);
                restauranteOld = em.merge(restauranteOld);
            }
            if (restauranteNew != null && !restauranteNew.equals(restauranteOld)) {
                restauranteNew.getReservasList().add(reservas);
                restauranteNew = em.merge(restauranteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = reservas.getIdReserva();
                if (findReservas(id) == null) {
                    throw new NonexistentEntityException("The reservas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Reservas reservas;
            try {
                reservas = em.getReference(Reservas.class, id);
                reservas.getIdReserva();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reservas with id " + id + " no longer exists.", enfe);
            }
            AreasRestaurante areaRestaurante = reservas.getAreaRestaurante();
            if (areaRestaurante != null) {
                areaRestaurante.getReservasList().remove(reservas);
                areaRestaurante = em.merge(areaRestaurante);
            }
            Usuarios nombrecliente = reservas.getNombrecliente();
            if (nombrecliente != null) {
                nombrecliente.getReservasList().remove(reservas);
                nombrecliente = em.merge(nombrecliente);
            }
            Restaurantes restaurante = reservas.getRestaurante();
            if (restaurante != null) {
                restaurante.getReservasList().remove(reservas);
                restaurante = em.merge(restaurante);
            }
            em.remove(reservas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Reservas> findReservasEntities() {
        return findReservasEntities(true, -1, -1);
    }

    public List<Reservas> findReservasEntities(int maxResults, int firstResult) {
        return findReservasEntities(false, maxResults, firstResult);
    }

    private List<Reservas> findReservasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Reservas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Reservas findReservas(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Reservas.class, id);
        } finally {
            em.close();
        }
    }

    public int getReservasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Reservas> rt = cq.from(Reservas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
