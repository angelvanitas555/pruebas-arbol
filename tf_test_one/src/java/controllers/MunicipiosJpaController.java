/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Departamentos;
import persistencia.Sucursales;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.Direcciones;
import persistencia.Municipios;

/**
 *
 * @author Angel
 */
public class MunicipiosJpaController implements Serializable {

    public MunicipiosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Municipios municipios) {
        if (municipios.getSucursalesList() == null) {
            municipios.setSucursalesList(new ArrayList<Sucursales>());
        }
        if (municipios.getDireccionesList() == null) {
            municipios.setDireccionesList(new ArrayList<Direcciones>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamentos departamento = municipios.getDepartamento();
            if (departamento != null) {
                departamento = em.getReference(departamento.getClass(), departamento.getIdDepartamento());
                municipios.setDepartamento(departamento);
            }
            List<Sucursales> attachedSucursalesList = new ArrayList<Sucursales>();
            for (Sucursales sucursalesListSucursalesToAttach : municipios.getSucursalesList()) {
                sucursalesListSucursalesToAttach = em.getReference(sucursalesListSucursalesToAttach.getClass(), sucursalesListSucursalesToAttach.getIdSucursal());
                attachedSucursalesList.add(sucursalesListSucursalesToAttach);
            }
            municipios.setSucursalesList(attachedSucursalesList);
            List<Direcciones> attachedDireccionesList = new ArrayList<Direcciones>();
            for (Direcciones direccionesListDireccionesToAttach : municipios.getDireccionesList()) {
                direccionesListDireccionesToAttach = em.getReference(direccionesListDireccionesToAttach.getClass(), direccionesListDireccionesToAttach.getIdDireccion());
                attachedDireccionesList.add(direccionesListDireccionesToAttach);
            }
            municipios.setDireccionesList(attachedDireccionesList);
            em.persist(municipios);
            if (departamento != null) {
                departamento.getMunicipiosList().add(municipios);
                departamento = em.merge(departamento);
            }
            for (Sucursales sucursalesListSucursales : municipios.getSucursalesList()) {
                Municipios oldMunicipioOfSucursalesListSucursales = sucursalesListSucursales.getMunicipio();
                sucursalesListSucursales.setMunicipio(municipios);
                sucursalesListSucursales = em.merge(sucursalesListSucursales);
                if (oldMunicipioOfSucursalesListSucursales != null) {
                    oldMunicipioOfSucursalesListSucursales.getSucursalesList().remove(sucursalesListSucursales);
                    oldMunicipioOfSucursalesListSucursales = em.merge(oldMunicipioOfSucursalesListSucursales);
                }
            }
            for (Direcciones direccionesListDirecciones : municipios.getDireccionesList()) {
                Municipios oldMunicipioOfDireccionesListDirecciones = direccionesListDirecciones.getMunicipio();
                direccionesListDirecciones.setMunicipio(municipios);
                direccionesListDirecciones = em.merge(direccionesListDirecciones);
                if (oldMunicipioOfDireccionesListDirecciones != null) {
                    oldMunicipioOfDireccionesListDirecciones.getDireccionesList().remove(direccionesListDirecciones);
                    oldMunicipioOfDireccionesListDirecciones = em.merge(oldMunicipioOfDireccionesListDirecciones);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Municipios municipios) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipios persistentMunicipios = em.find(Municipios.class, municipios.getIdMunicipio());
            Departamentos departamentoOld = persistentMunicipios.getDepartamento();
            Departamentos departamentoNew = municipios.getDepartamento();
            List<Sucursales> sucursalesListOld = persistentMunicipios.getSucursalesList();
            List<Sucursales> sucursalesListNew = municipios.getSucursalesList();
            List<Direcciones> direccionesListOld = persistentMunicipios.getDireccionesList();
            List<Direcciones> direccionesListNew = municipios.getDireccionesList();
            if (departamentoNew != null) {
                departamentoNew = em.getReference(departamentoNew.getClass(), departamentoNew.getIdDepartamento());
                municipios.setDepartamento(departamentoNew);
            }
            List<Sucursales> attachedSucursalesListNew = new ArrayList<Sucursales>();
            for (Sucursales sucursalesListNewSucursalesToAttach : sucursalesListNew) {
                sucursalesListNewSucursalesToAttach = em.getReference(sucursalesListNewSucursalesToAttach.getClass(), sucursalesListNewSucursalesToAttach.getIdSucursal());
                attachedSucursalesListNew.add(sucursalesListNewSucursalesToAttach);
            }
            sucursalesListNew = attachedSucursalesListNew;
            municipios.setSucursalesList(sucursalesListNew);
            List<Direcciones> attachedDireccionesListNew = new ArrayList<Direcciones>();
            for (Direcciones direccionesListNewDireccionesToAttach : direccionesListNew) {
                direccionesListNewDireccionesToAttach = em.getReference(direccionesListNewDireccionesToAttach.getClass(), direccionesListNewDireccionesToAttach.getIdDireccion());
                attachedDireccionesListNew.add(direccionesListNewDireccionesToAttach);
            }
            direccionesListNew = attachedDireccionesListNew;
            municipios.setDireccionesList(direccionesListNew);
            municipios = em.merge(municipios);
            if (departamentoOld != null && !departamentoOld.equals(departamentoNew)) {
                departamentoOld.getMunicipiosList().remove(municipios);
                departamentoOld = em.merge(departamentoOld);
            }
            if (departamentoNew != null && !departamentoNew.equals(departamentoOld)) {
                departamentoNew.getMunicipiosList().add(municipios);
                departamentoNew = em.merge(departamentoNew);
            }
            for (Sucursales sucursalesListOldSucursales : sucursalesListOld) {
                if (!sucursalesListNew.contains(sucursalesListOldSucursales)) {
                    sucursalesListOldSucursales.setMunicipio(null);
                    sucursalesListOldSucursales = em.merge(sucursalesListOldSucursales);
                }
            }
            for (Sucursales sucursalesListNewSucursales : sucursalesListNew) {
                if (!sucursalesListOld.contains(sucursalesListNewSucursales)) {
                    Municipios oldMunicipioOfSucursalesListNewSucursales = sucursalesListNewSucursales.getMunicipio();
                    sucursalesListNewSucursales.setMunicipio(municipios);
                    sucursalesListNewSucursales = em.merge(sucursalesListNewSucursales);
                    if (oldMunicipioOfSucursalesListNewSucursales != null && !oldMunicipioOfSucursalesListNewSucursales.equals(municipios)) {
                        oldMunicipioOfSucursalesListNewSucursales.getSucursalesList().remove(sucursalesListNewSucursales);
                        oldMunicipioOfSucursalesListNewSucursales = em.merge(oldMunicipioOfSucursalesListNewSucursales);
                    }
                }
            }
            for (Direcciones direccionesListOldDirecciones : direccionesListOld) {
                if (!direccionesListNew.contains(direccionesListOldDirecciones)) {
                    direccionesListOldDirecciones.setMunicipio(null);
                    direccionesListOldDirecciones = em.merge(direccionesListOldDirecciones);
                }
            }
            for (Direcciones direccionesListNewDirecciones : direccionesListNew) {
                if (!direccionesListOld.contains(direccionesListNewDirecciones)) {
                    Municipios oldMunicipioOfDireccionesListNewDirecciones = direccionesListNewDirecciones.getMunicipio();
                    direccionesListNewDirecciones.setMunicipio(municipios);
                    direccionesListNewDirecciones = em.merge(direccionesListNewDirecciones);
                    if (oldMunicipioOfDireccionesListNewDirecciones != null && !oldMunicipioOfDireccionesListNewDirecciones.equals(municipios)) {
                        oldMunicipioOfDireccionesListNewDirecciones.getDireccionesList().remove(direccionesListNewDirecciones);
                        oldMunicipioOfDireccionesListNewDirecciones = em.merge(oldMunicipioOfDireccionesListNewDirecciones);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = municipios.getIdMunicipio();
                if (findMunicipios(id) == null) {
                    throw new NonexistentEntityException("The municipios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipios municipios;
            try {
                municipios = em.getReference(Municipios.class, id);
                municipios.getIdMunicipio();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The municipios with id " + id + " no longer exists.", enfe);
            }
            Departamentos departamento = municipios.getDepartamento();
            if (departamento != null) {
                departamento.getMunicipiosList().remove(municipios);
                departamento = em.merge(departamento);
            }
            List<Sucursales> sucursalesList = municipios.getSucursalesList();
            for (Sucursales sucursalesListSucursales : sucursalesList) {
                sucursalesListSucursales.setMunicipio(null);
                sucursalesListSucursales = em.merge(sucursalesListSucursales);
            }
            List<Direcciones> direccionesList = municipios.getDireccionesList();
            for (Direcciones direccionesListDirecciones : direccionesList) {
                direccionesListDirecciones.setMunicipio(null);
                direccionesListDirecciones = em.merge(direccionesListDirecciones);
            }
            em.remove(municipios);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Municipios> findMunicipiosEntities() {
        return findMunicipiosEntities(true, -1, -1);
    }

    public List<Municipios> findMunicipiosEntities(int maxResults, int firstResult) {
        return findMunicipiosEntities(false, maxResults, firstResult);
    }

    private List<Municipios> findMunicipiosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Municipios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Municipios findMunicipios(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Municipios.class, id);
        } finally {
            em.close();
        }
    }

    public int getMunicipiosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Municipios> rt = cq.from(Municipios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
