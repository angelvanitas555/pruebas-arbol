/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Municipios;
import persistencia.Departamentos;
import persistencia.Restaurantes;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.Direcciones;

/**
 *
 * @author Angel
 */
public class DireccionesJpaController implements Serializable {

    public DireccionesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Direcciones direcciones) {
        if (direcciones.getRestaurantesList() == null) {
            direcciones.setRestaurantesList(new ArrayList<Restaurantes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipios municipio = direcciones.getMunicipio();
            if (municipio != null) {
                municipio = em.getReference(municipio.getClass(), municipio.getIdMunicipio());
                direcciones.setMunicipio(municipio);
            }
            Departamentos departamento = direcciones.getDepartamento();
            if (departamento != null) {
                departamento = em.getReference(departamento.getClass(), departamento.getIdDepartamento());
                direcciones.setDepartamento(departamento);
            }
            List<Restaurantes> attachedRestaurantesList = new ArrayList<Restaurantes>();
            for (Restaurantes restaurantesListRestaurantesToAttach : direcciones.getRestaurantesList()) {
                restaurantesListRestaurantesToAttach = em.getReference(restaurantesListRestaurantesToAttach.getClass(), restaurantesListRestaurantesToAttach.getIdRestaurante());
                attachedRestaurantesList.add(restaurantesListRestaurantesToAttach);
            }
            direcciones.setRestaurantesList(attachedRestaurantesList);
            em.persist(direcciones);
            if (municipio != null) {
                municipio.getDireccionesList().add(direcciones);
                municipio = em.merge(municipio);
            }
            if (departamento != null) {
                departamento.getDireccionesList().add(direcciones);
                departamento = em.merge(departamento);
            }
            for (Restaurantes restaurantesListRestaurantes : direcciones.getRestaurantesList()) {
                Direcciones oldDireccionOfRestaurantesListRestaurantes = restaurantesListRestaurantes.getDireccion();
                restaurantesListRestaurantes.setDireccion(direcciones);
                restaurantesListRestaurantes = em.merge(restaurantesListRestaurantes);
                if (oldDireccionOfRestaurantesListRestaurantes != null) {
                    oldDireccionOfRestaurantesListRestaurantes.getRestaurantesList().remove(restaurantesListRestaurantes);
                    oldDireccionOfRestaurantesListRestaurantes = em.merge(oldDireccionOfRestaurantesListRestaurantes);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Direcciones direcciones) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direcciones persistentDirecciones = em.find(Direcciones.class, direcciones.getIdDireccion());
            Municipios municipioOld = persistentDirecciones.getMunicipio();
            Municipios municipioNew = direcciones.getMunicipio();
            Departamentos departamentoOld = persistentDirecciones.getDepartamento();
            Departamentos departamentoNew = direcciones.getDepartamento();
            List<Restaurantes> restaurantesListOld = persistentDirecciones.getRestaurantesList();
            List<Restaurantes> restaurantesListNew = direcciones.getRestaurantesList();
            if (municipioNew != null) {
                municipioNew = em.getReference(municipioNew.getClass(), municipioNew.getIdMunicipio());
                direcciones.setMunicipio(municipioNew);
            }
            if (departamentoNew != null) {
                departamentoNew = em.getReference(departamentoNew.getClass(), departamentoNew.getIdDepartamento());
                direcciones.setDepartamento(departamentoNew);
            }
            List<Restaurantes> attachedRestaurantesListNew = new ArrayList<Restaurantes>();
            for (Restaurantes restaurantesListNewRestaurantesToAttach : restaurantesListNew) {
                restaurantesListNewRestaurantesToAttach = em.getReference(restaurantesListNewRestaurantesToAttach.getClass(), restaurantesListNewRestaurantesToAttach.getIdRestaurante());
                attachedRestaurantesListNew.add(restaurantesListNewRestaurantesToAttach);
            }
            restaurantesListNew = attachedRestaurantesListNew;
            direcciones.setRestaurantesList(restaurantesListNew);
            direcciones = em.merge(direcciones);
            if (municipioOld != null && !municipioOld.equals(municipioNew)) {
                municipioOld.getDireccionesList().remove(direcciones);
                municipioOld = em.merge(municipioOld);
            }
            if (municipioNew != null && !municipioNew.equals(municipioOld)) {
                municipioNew.getDireccionesList().add(direcciones);
                municipioNew = em.merge(municipioNew);
            }
            if (departamentoOld != null && !departamentoOld.equals(departamentoNew)) {
                departamentoOld.getDireccionesList().remove(direcciones);
                departamentoOld = em.merge(departamentoOld);
            }
            if (departamentoNew != null && !departamentoNew.equals(departamentoOld)) {
                departamentoNew.getDireccionesList().add(direcciones);
                departamentoNew = em.merge(departamentoNew);
            }
            for (Restaurantes restaurantesListOldRestaurantes : restaurantesListOld) {
                if (!restaurantesListNew.contains(restaurantesListOldRestaurantes)) {
                    restaurantesListOldRestaurantes.setDireccion(null);
                    restaurantesListOldRestaurantes = em.merge(restaurantesListOldRestaurantes);
                }
            }
            for (Restaurantes restaurantesListNewRestaurantes : restaurantesListNew) {
                if (!restaurantesListOld.contains(restaurantesListNewRestaurantes)) {
                    Direcciones oldDireccionOfRestaurantesListNewRestaurantes = restaurantesListNewRestaurantes.getDireccion();
                    restaurantesListNewRestaurantes.setDireccion(direcciones);
                    restaurantesListNewRestaurantes = em.merge(restaurantesListNewRestaurantes);
                    if (oldDireccionOfRestaurantesListNewRestaurantes != null && !oldDireccionOfRestaurantesListNewRestaurantes.equals(direcciones)) {
                        oldDireccionOfRestaurantesListNewRestaurantes.getRestaurantesList().remove(restaurantesListNewRestaurantes);
                        oldDireccionOfRestaurantesListNewRestaurantes = em.merge(oldDireccionOfRestaurantesListNewRestaurantes);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = direcciones.getIdDireccion();
                if (findDirecciones(id) == null) {
                    throw new NonexistentEntityException("The direcciones with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direcciones direcciones;
            try {
                direcciones = em.getReference(Direcciones.class, id);
                direcciones.getIdDireccion();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The direcciones with id " + id + " no longer exists.", enfe);
            }
            Municipios municipio = direcciones.getMunicipio();
            if (municipio != null) {
                municipio.getDireccionesList().remove(direcciones);
                municipio = em.merge(municipio);
            }
            Departamentos departamento = direcciones.getDepartamento();
            if (departamento != null) {
                departamento.getDireccionesList().remove(direcciones);
                departamento = em.merge(departamento);
            }
            List<Restaurantes> restaurantesList = direcciones.getRestaurantesList();
            for (Restaurantes restaurantesListRestaurantes : restaurantesList) {
                restaurantesListRestaurantes.setDireccion(null);
                restaurantesListRestaurantes = em.merge(restaurantesListRestaurantes);
            }
            em.remove(direcciones);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Direcciones> findDireccionesEntities() {
        return findDireccionesEntities(true, -1, -1);
    }

    public List<Direcciones> findDireccionesEntities(int maxResults, int firstResult) {
        return findDireccionesEntities(false, maxResults, firstResult);
    }

    private List<Direcciones> findDireccionesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Direcciones.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Direcciones findDirecciones(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Direcciones.class, id);
        } finally {
            em.close();
        }
    }

    public int getDireccionesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Direcciones> rt = cq.from(Direcciones.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
