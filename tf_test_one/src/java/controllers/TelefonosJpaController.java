/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Restaurantes;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.Telefonos;

/**
 *
 * @author Angel
 */
public class TelefonosJpaController implements Serializable {

    public TelefonosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Telefonos telefonos) {
        if (telefonos.getRestaurantesList() == null) {
            telefonos.setRestaurantesList(new ArrayList<Restaurantes>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Restaurantes> attachedRestaurantesList = new ArrayList<Restaurantes>();
            for (Restaurantes restaurantesListRestaurantesToAttach : telefonos.getRestaurantesList()) {
                restaurantesListRestaurantesToAttach = em.getReference(restaurantesListRestaurantesToAttach.getClass(), restaurantesListRestaurantesToAttach.getIdRestaurante());
                attachedRestaurantesList.add(restaurantesListRestaurantesToAttach);
            }
            telefonos.setRestaurantesList(attachedRestaurantesList);
            em.persist(telefonos);
            for (Restaurantes restaurantesListRestaurantes : telefonos.getRestaurantesList()) {
                Telefonos oldTelefonoOfRestaurantesListRestaurantes = restaurantesListRestaurantes.getTelefono();
                restaurantesListRestaurantes.setTelefono(telefonos);
                restaurantesListRestaurantes = em.merge(restaurantesListRestaurantes);
                if (oldTelefonoOfRestaurantesListRestaurantes != null) {
                    oldTelefonoOfRestaurantesListRestaurantes.getRestaurantesList().remove(restaurantesListRestaurantes);
                    oldTelefonoOfRestaurantesListRestaurantes = em.merge(oldTelefonoOfRestaurantesListRestaurantes);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Telefonos telefonos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Telefonos persistentTelefonos = em.find(Telefonos.class, telefonos.getIdTelefono());
            List<Restaurantes> restaurantesListOld = persistentTelefonos.getRestaurantesList();
            List<Restaurantes> restaurantesListNew = telefonos.getRestaurantesList();
            List<Restaurantes> attachedRestaurantesListNew = new ArrayList<Restaurantes>();
            for (Restaurantes restaurantesListNewRestaurantesToAttach : restaurantesListNew) {
                restaurantesListNewRestaurantesToAttach = em.getReference(restaurantesListNewRestaurantesToAttach.getClass(), restaurantesListNewRestaurantesToAttach.getIdRestaurante());
                attachedRestaurantesListNew.add(restaurantesListNewRestaurantesToAttach);
            }
            restaurantesListNew = attachedRestaurantesListNew;
            telefonos.setRestaurantesList(restaurantesListNew);
            telefonos = em.merge(telefonos);
            for (Restaurantes restaurantesListOldRestaurantes : restaurantesListOld) {
                if (!restaurantesListNew.contains(restaurantesListOldRestaurantes)) {
                    restaurantesListOldRestaurantes.setTelefono(null);
                    restaurantesListOldRestaurantes = em.merge(restaurantesListOldRestaurantes);
                }
            }
            for (Restaurantes restaurantesListNewRestaurantes : restaurantesListNew) {
                if (!restaurantesListOld.contains(restaurantesListNewRestaurantes)) {
                    Telefonos oldTelefonoOfRestaurantesListNewRestaurantes = restaurantesListNewRestaurantes.getTelefono();
                    restaurantesListNewRestaurantes.setTelefono(telefonos);
                    restaurantesListNewRestaurantes = em.merge(restaurantesListNewRestaurantes);
                    if (oldTelefonoOfRestaurantesListNewRestaurantes != null && !oldTelefonoOfRestaurantesListNewRestaurantes.equals(telefonos)) {
                        oldTelefonoOfRestaurantesListNewRestaurantes.getRestaurantesList().remove(restaurantesListNewRestaurantes);
                        oldTelefonoOfRestaurantesListNewRestaurantes = em.merge(oldTelefonoOfRestaurantesListNewRestaurantes);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = telefonos.getIdTelefono();
                if (findTelefonos(id) == null) {
                    throw new NonexistentEntityException("The telefonos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Telefonos telefonos;
            try {
                telefonos = em.getReference(Telefonos.class, id);
                telefonos.getIdTelefono();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The telefonos with id " + id + " no longer exists.", enfe);
            }
            List<Restaurantes> restaurantesList = telefonos.getRestaurantesList();
            for (Restaurantes restaurantesListRestaurantes : restaurantesList) {
                restaurantesListRestaurantes.setTelefono(null);
                restaurantesListRestaurantes = em.merge(restaurantesListRestaurantes);
            }
            em.remove(telefonos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Telefonos> findTelefonosEntities() {
        return findTelefonosEntities(true, -1, -1);
    }

    public List<Telefonos> findTelefonosEntities(int maxResults, int firstResult) {
        return findTelefonosEntities(false, maxResults, firstResult);
    }

    private List<Telefonos> findTelefonosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Telefonos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Telefonos findTelefonos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Telefonos.class, id);
        } finally {
            em.close();
        }
    }

    public int getTelefonosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Telefonos> rt = cq.from(Telefonos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
