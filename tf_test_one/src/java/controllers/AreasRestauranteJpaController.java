/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Reservas;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.AreasRestaurante;

/**
 *
 * @author Angel
 */
public class AreasRestauranteJpaController implements Serializable {

    public AreasRestauranteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AreasRestaurante areasRestaurante) {
        if (areasRestaurante.getReservasList() == null) {
            areasRestaurante.setReservasList(new ArrayList<Reservas>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Reservas> attachedReservasList = new ArrayList<Reservas>();
            for (Reservas reservasListReservasToAttach : areasRestaurante.getReservasList()) {
                reservasListReservasToAttach = em.getReference(reservasListReservasToAttach.getClass(), reservasListReservasToAttach.getIdReserva());
                attachedReservasList.add(reservasListReservasToAttach);
            }
            areasRestaurante.setReservasList(attachedReservasList);
            em.persist(areasRestaurante);
            for (Reservas reservasListReservas : areasRestaurante.getReservasList()) {
                AreasRestaurante oldAreaRestauranteOfReservasListReservas = reservasListReservas.getAreaRestaurante();
                reservasListReservas.setAreaRestaurante(areasRestaurante);
                reservasListReservas = em.merge(reservasListReservas);
                if (oldAreaRestauranteOfReservasListReservas != null) {
                    oldAreaRestauranteOfReservasListReservas.getReservasList().remove(reservasListReservas);
                    oldAreaRestauranteOfReservasListReservas = em.merge(oldAreaRestauranteOfReservasListReservas);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AreasRestaurante areasRestaurante) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AreasRestaurante persistentAreasRestaurante = em.find(AreasRestaurante.class, areasRestaurante.getIdArea());
            List<Reservas> reservasListOld = persistentAreasRestaurante.getReservasList();
            List<Reservas> reservasListNew = areasRestaurante.getReservasList();
            List<Reservas> attachedReservasListNew = new ArrayList<Reservas>();
            for (Reservas reservasListNewReservasToAttach : reservasListNew) {
                reservasListNewReservasToAttach = em.getReference(reservasListNewReservasToAttach.getClass(), reservasListNewReservasToAttach.getIdReserva());
                attachedReservasListNew.add(reservasListNewReservasToAttach);
            }
            reservasListNew = attachedReservasListNew;
            areasRestaurante.setReservasList(reservasListNew);
            areasRestaurante = em.merge(areasRestaurante);
            for (Reservas reservasListOldReservas : reservasListOld) {
                if (!reservasListNew.contains(reservasListOldReservas)) {
                    reservasListOldReservas.setAreaRestaurante(null);
                    reservasListOldReservas = em.merge(reservasListOldReservas);
                }
            }
            for (Reservas reservasListNewReservas : reservasListNew) {
                if (!reservasListOld.contains(reservasListNewReservas)) {
                    AreasRestaurante oldAreaRestauranteOfReservasListNewReservas = reservasListNewReservas.getAreaRestaurante();
                    reservasListNewReservas.setAreaRestaurante(areasRestaurante);
                    reservasListNewReservas = em.merge(reservasListNewReservas);
                    if (oldAreaRestauranteOfReservasListNewReservas != null && !oldAreaRestauranteOfReservasListNewReservas.equals(areasRestaurante)) {
                        oldAreaRestauranteOfReservasListNewReservas.getReservasList().remove(reservasListNewReservas);
                        oldAreaRestauranteOfReservasListNewReservas = em.merge(oldAreaRestauranteOfReservasListNewReservas);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = areasRestaurante.getIdArea();
                if (findAreasRestaurante(id) == null) {
                    throw new NonexistentEntityException("The areasRestaurante with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AreasRestaurante areasRestaurante;
            try {
                areasRestaurante = em.getReference(AreasRestaurante.class, id);
                areasRestaurante.getIdArea();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The areasRestaurante with id " + id + " no longer exists.", enfe);
            }
            List<Reservas> reservasList = areasRestaurante.getReservasList();
            for (Reservas reservasListReservas : reservasList) {
                reservasListReservas.setAreaRestaurante(null);
                reservasListReservas = em.merge(reservasListReservas);
            }
            em.remove(areasRestaurante);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AreasRestaurante> findAreasRestauranteEntities() {
        return findAreasRestauranteEntities(true, -1, -1);
    }

    public List<AreasRestaurante> findAreasRestauranteEntities(int maxResults, int firstResult) {
        return findAreasRestauranteEntities(false, maxResults, firstResult);
    }

    private List<AreasRestaurante> findAreasRestauranteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AreasRestaurante.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AreasRestaurante findAreasRestaurante(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AreasRestaurante.class, id);
        } finally {
            em.close();
        }
    }

    public int getAreasRestauranteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AreasRestaurante> rt = cq.from(AreasRestaurante.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
