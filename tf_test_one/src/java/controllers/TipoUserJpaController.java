/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Usuarios;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.TipoUser;

/**
 *
 * @author Angel
 */
public class TipoUserJpaController implements Serializable {

    public TipoUserJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TipoUser tipoUser) {
        if (tipoUser.getUsuariosList() == null) {
            tipoUser.setUsuariosList(new ArrayList<Usuarios>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Usuarios> attachedUsuariosList = new ArrayList<Usuarios>();
            for (Usuarios usuariosListUsuariosToAttach : tipoUser.getUsuariosList()) {
                usuariosListUsuariosToAttach = em.getReference(usuariosListUsuariosToAttach.getClass(), usuariosListUsuariosToAttach.getIdUsuario());
                attachedUsuariosList.add(usuariosListUsuariosToAttach);
            }
            tipoUser.setUsuariosList(attachedUsuariosList);
            em.persist(tipoUser);
            for (Usuarios usuariosListUsuarios : tipoUser.getUsuariosList()) {
                TipoUser oldTipoUserOfUsuariosListUsuarios = usuariosListUsuarios.getTipoUser();
                usuariosListUsuarios.setTipoUser(tipoUser);
                usuariosListUsuarios = em.merge(usuariosListUsuarios);
                if (oldTipoUserOfUsuariosListUsuarios != null) {
                    oldTipoUserOfUsuariosListUsuarios.getUsuariosList().remove(usuariosListUsuarios);
                    oldTipoUserOfUsuariosListUsuarios = em.merge(oldTipoUserOfUsuariosListUsuarios);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TipoUser tipoUser) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoUser persistentTipoUser = em.find(TipoUser.class, tipoUser.getIdTipoUser());
            List<Usuarios> usuariosListOld = persistentTipoUser.getUsuariosList();
            List<Usuarios> usuariosListNew = tipoUser.getUsuariosList();
            List<Usuarios> attachedUsuariosListNew = new ArrayList<Usuarios>();
            for (Usuarios usuariosListNewUsuariosToAttach : usuariosListNew) {
                usuariosListNewUsuariosToAttach = em.getReference(usuariosListNewUsuariosToAttach.getClass(), usuariosListNewUsuariosToAttach.getIdUsuario());
                attachedUsuariosListNew.add(usuariosListNewUsuariosToAttach);
            }
            usuariosListNew = attachedUsuariosListNew;
            tipoUser.setUsuariosList(usuariosListNew);
            tipoUser = em.merge(tipoUser);
            for (Usuarios usuariosListOldUsuarios : usuariosListOld) {
                if (!usuariosListNew.contains(usuariosListOldUsuarios)) {
                    usuariosListOldUsuarios.setTipoUser(null);
                    usuariosListOldUsuarios = em.merge(usuariosListOldUsuarios);
                }
            }
            for (Usuarios usuariosListNewUsuarios : usuariosListNew) {
                if (!usuariosListOld.contains(usuariosListNewUsuarios)) {
                    TipoUser oldTipoUserOfUsuariosListNewUsuarios = usuariosListNewUsuarios.getTipoUser();
                    usuariosListNewUsuarios.setTipoUser(tipoUser);
                    usuariosListNewUsuarios = em.merge(usuariosListNewUsuarios);
                    if (oldTipoUserOfUsuariosListNewUsuarios != null && !oldTipoUserOfUsuariosListNewUsuarios.equals(tipoUser)) {
                        oldTipoUserOfUsuariosListNewUsuarios.getUsuariosList().remove(usuariosListNewUsuarios);
                        oldTipoUserOfUsuariosListNewUsuarios = em.merge(oldTipoUserOfUsuariosListNewUsuarios);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tipoUser.getIdTipoUser();
                if (findTipoUser(id) == null) {
                    throw new NonexistentEntityException("The tipoUser with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoUser tipoUser;
            try {
                tipoUser = em.getReference(TipoUser.class, id);
                tipoUser.getIdTipoUser();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tipoUser with id " + id + " no longer exists.", enfe);
            }
            List<Usuarios> usuariosList = tipoUser.getUsuariosList();
            for (Usuarios usuariosListUsuarios : usuariosList) {
                usuariosListUsuarios.setTipoUser(null);
                usuariosListUsuarios = em.merge(usuariosListUsuarios);
            }
            em.remove(tipoUser);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TipoUser> findTipoUserEntities() {
        return findTipoUserEntities(true, -1, -1);
    }

    public List<TipoUser> findTipoUserEntities(int maxResults, int firstResult) {
        return findTipoUserEntities(false, maxResults, firstResult);
    }

    private List<TipoUser> findTipoUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TipoUser.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TipoUser findTipoUser(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TipoUser.class, id);
        } finally {
            em.close();
        }
    }

    public int getTipoUserCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TipoUser> rt = cq.from(TipoUser.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
