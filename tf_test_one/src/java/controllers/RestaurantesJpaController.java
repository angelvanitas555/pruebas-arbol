/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Direcciones;
import persistencia.Telefonos;
import persistencia.SucuralRestaurante;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.Reservas;
import persistencia.Restaurantes;

/**
 *
 * @author Angel
 */
public class RestaurantesJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public RestaurantesJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tf_test_onePU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Restaurantes restaurantes) {
        if (restaurantes.getSucuralRestauranteList() == null) {
            restaurantes.setSucuralRestauranteList(new ArrayList<SucuralRestaurante>());
        }
        if (restaurantes.getReservasList() == null) {
            restaurantes.setReservasList(new ArrayList<Reservas>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Direcciones direccion = restaurantes.getDireccion();
            if (direccion != null) {
                direccion = em.getReference(direccion.getClass(), direccion.getIdDireccion());
                restaurantes.setDireccion(direccion);
            }
            Telefonos telefono = restaurantes.getTelefono();
            if (telefono != null) {
                telefono = em.getReference(telefono.getClass(), telefono.getIdTelefono());
                restaurantes.setTelefono(telefono);
            }
            List<SucuralRestaurante> attachedSucuralRestauranteList = new ArrayList<SucuralRestaurante>();
            for (SucuralRestaurante sucuralRestauranteListSucuralRestauranteToAttach : restaurantes.getSucuralRestauranteList()) {
                sucuralRestauranteListSucuralRestauranteToAttach = em.getReference(sucuralRestauranteListSucuralRestauranteToAttach.getClass(), sucuralRestauranteListSucuralRestauranteToAttach.getIdPivoSucursalRestaurante());
                attachedSucuralRestauranteList.add(sucuralRestauranteListSucuralRestauranteToAttach);
            }
            restaurantes.setSucuralRestauranteList(attachedSucuralRestauranteList);
            List<Reservas> attachedReservasList = new ArrayList<Reservas>();
            for (Reservas reservasListReservasToAttach : restaurantes.getReservasList()) {
                reservasListReservasToAttach = em.getReference(reservasListReservasToAttach.getClass(), reservasListReservasToAttach.getIdReserva());
                attachedReservasList.add(reservasListReservasToAttach);
            }
            restaurantes.setReservasList(attachedReservasList);
            em.persist(restaurantes);
            if (direccion != null) {
                direccion.getRestaurantesList().add(restaurantes);
                direccion = em.merge(direccion);
            }
            if (telefono != null) {
                telefono.getRestaurantesList().add(restaurantes);
                telefono = em.merge(telefono);
            }
            for (SucuralRestaurante sucuralRestauranteListSucuralRestaurante : restaurantes.getSucuralRestauranteList()) {
                Restaurantes oldIdRestauranteOfSucuralRestauranteListSucuralRestaurante = sucuralRestauranteListSucuralRestaurante.getIdRestaurante();
                sucuralRestauranteListSucuralRestaurante.setIdRestaurante(restaurantes);
                sucuralRestauranteListSucuralRestaurante = em.merge(sucuralRestauranteListSucuralRestaurante);
                if (oldIdRestauranteOfSucuralRestauranteListSucuralRestaurante != null) {
                    oldIdRestauranteOfSucuralRestauranteListSucuralRestaurante.getSucuralRestauranteList().remove(sucuralRestauranteListSucuralRestaurante);
                    oldIdRestauranteOfSucuralRestauranteListSucuralRestaurante = em.merge(oldIdRestauranteOfSucuralRestauranteListSucuralRestaurante);
                }
            }
            for (Reservas reservasListReservas : restaurantes.getReservasList()) {
                Restaurantes oldRestauranteOfReservasListReservas = reservasListReservas.getRestaurante();
                reservasListReservas.setRestaurante(restaurantes);
                reservasListReservas = em.merge(reservasListReservas);
                if (oldRestauranteOfReservasListReservas != null) {
                    oldRestauranteOfReservasListReservas.getReservasList().remove(reservasListReservas);
                    oldRestauranteOfReservasListReservas = em.merge(oldRestauranteOfReservasListReservas);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Restaurantes restaurantes) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Restaurantes persistentRestaurantes = em.find(Restaurantes.class, restaurantes.getIdRestaurante());
            Direcciones direccionOld = persistentRestaurantes.getDireccion();
            Direcciones direccionNew = restaurantes.getDireccion();
            Telefonos telefonoOld = persistentRestaurantes.getTelefono();
            Telefonos telefonoNew = restaurantes.getTelefono();
            List<SucuralRestaurante> sucuralRestauranteListOld = persistentRestaurantes.getSucuralRestauranteList();
            List<SucuralRestaurante> sucuralRestauranteListNew = restaurantes.getSucuralRestauranteList();
            List<Reservas> reservasListOld = persistentRestaurantes.getReservasList();
            List<Reservas> reservasListNew = restaurantes.getReservasList();
            if (direccionNew != null) {
                direccionNew = em.getReference(direccionNew.getClass(), direccionNew.getIdDireccion());
                restaurantes.setDireccion(direccionNew);
            }
            if (telefonoNew != null) {
                telefonoNew = em.getReference(telefonoNew.getClass(), telefonoNew.getIdTelefono());
                restaurantes.setTelefono(telefonoNew);
            }
            List<SucuralRestaurante> attachedSucuralRestauranteListNew = new ArrayList<SucuralRestaurante>();
            for (SucuralRestaurante sucuralRestauranteListNewSucuralRestauranteToAttach : sucuralRestauranteListNew) {
                sucuralRestauranteListNewSucuralRestauranteToAttach = em.getReference(sucuralRestauranteListNewSucuralRestauranteToAttach.getClass(), sucuralRestauranteListNewSucuralRestauranteToAttach.getIdPivoSucursalRestaurante());
                attachedSucuralRestauranteListNew.add(sucuralRestauranteListNewSucuralRestauranteToAttach);
            }
            sucuralRestauranteListNew = attachedSucuralRestauranteListNew;
            restaurantes.setSucuralRestauranteList(sucuralRestauranteListNew);
            List<Reservas> attachedReservasListNew = new ArrayList<Reservas>();
            for (Reservas reservasListNewReservasToAttach : reservasListNew) {
                reservasListNewReservasToAttach = em.getReference(reservasListNewReservasToAttach.getClass(), reservasListNewReservasToAttach.getIdReserva());
                attachedReservasListNew.add(reservasListNewReservasToAttach);
            }
            reservasListNew = attachedReservasListNew;
            restaurantes.setReservasList(reservasListNew);
            restaurantes = em.merge(restaurantes);
            if (direccionOld != null && !direccionOld.equals(direccionNew)) {
                direccionOld.getRestaurantesList().remove(restaurantes);
                direccionOld = em.merge(direccionOld);
            }
            if (direccionNew != null && !direccionNew.equals(direccionOld)) {
                direccionNew.getRestaurantesList().add(restaurantes);
                direccionNew = em.merge(direccionNew);
            }
            if (telefonoOld != null && !telefonoOld.equals(telefonoNew)) {
                telefonoOld.getRestaurantesList().remove(restaurantes);
                telefonoOld = em.merge(telefonoOld);
            }
            if (telefonoNew != null && !telefonoNew.equals(telefonoOld)) {
                telefonoNew.getRestaurantesList().add(restaurantes);
                telefonoNew = em.merge(telefonoNew);
            }
            for (SucuralRestaurante sucuralRestauranteListOldSucuralRestaurante : sucuralRestauranteListOld) {
                if (!sucuralRestauranteListNew.contains(sucuralRestauranteListOldSucuralRestaurante)) {
                    sucuralRestauranteListOldSucuralRestaurante.setIdRestaurante(null);
                    sucuralRestauranteListOldSucuralRestaurante = em.merge(sucuralRestauranteListOldSucuralRestaurante);
                }
            }
            for (SucuralRestaurante sucuralRestauranteListNewSucuralRestaurante : sucuralRestauranteListNew) {
                if (!sucuralRestauranteListOld.contains(sucuralRestauranteListNewSucuralRestaurante)) {
                    Restaurantes oldIdRestauranteOfSucuralRestauranteListNewSucuralRestaurante = sucuralRestauranteListNewSucuralRestaurante.getIdRestaurante();
                    sucuralRestauranteListNewSucuralRestaurante.setIdRestaurante(restaurantes);
                    sucuralRestauranteListNewSucuralRestaurante = em.merge(sucuralRestauranteListNewSucuralRestaurante);
                    if (oldIdRestauranteOfSucuralRestauranteListNewSucuralRestaurante != null && !oldIdRestauranteOfSucuralRestauranteListNewSucuralRestaurante.equals(restaurantes)) {
                        oldIdRestauranteOfSucuralRestauranteListNewSucuralRestaurante.getSucuralRestauranteList().remove(sucuralRestauranteListNewSucuralRestaurante);
                        oldIdRestauranteOfSucuralRestauranteListNewSucuralRestaurante = em.merge(oldIdRestauranteOfSucuralRestauranteListNewSucuralRestaurante);
                    }
                }
            }
            for (Reservas reservasListOldReservas : reservasListOld) {
                if (!reservasListNew.contains(reservasListOldReservas)) {
                    reservasListOldReservas.setRestaurante(null);
                    reservasListOldReservas = em.merge(reservasListOldReservas);
                }
            }
            for (Reservas reservasListNewReservas : reservasListNew) {
                if (!reservasListOld.contains(reservasListNewReservas)) {
                    Restaurantes oldRestauranteOfReservasListNewReservas = reservasListNewReservas.getRestaurante();
                    reservasListNewReservas.setRestaurante(restaurantes);
                    reservasListNewReservas = em.merge(reservasListNewReservas);
                    if (oldRestauranteOfReservasListNewReservas != null && !oldRestauranteOfReservasListNewReservas.equals(restaurantes)) {
                        oldRestauranteOfReservasListNewReservas.getReservasList().remove(reservasListNewReservas);
                        oldRestauranteOfReservasListNewReservas = em.merge(oldRestauranteOfReservasListNewReservas);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = restaurantes.getIdRestaurante();
                if (findRestaurantes(id) == null) {
                    throw new NonexistentEntityException("The restaurantes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Restaurantes restaurantes;
            try {
                restaurantes = em.getReference(Restaurantes.class, id);
                restaurantes.getIdRestaurante();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The restaurantes with id " + id + " no longer exists.", enfe);
            }
            Direcciones direccion = restaurantes.getDireccion();
            if (direccion != null) {
                direccion.getRestaurantesList().remove(restaurantes);
                direccion = em.merge(direccion);
            }
            Telefonos telefono = restaurantes.getTelefono();
            if (telefono != null) {
                telefono.getRestaurantesList().remove(restaurantes);
                telefono = em.merge(telefono);
            }
            List<SucuralRestaurante> sucuralRestauranteList = restaurantes.getSucuralRestauranteList();
            for (SucuralRestaurante sucuralRestauranteListSucuralRestaurante : sucuralRestauranteList) {
                sucuralRestauranteListSucuralRestaurante.setIdRestaurante(null);
                sucuralRestauranteListSucuralRestaurante = em.merge(sucuralRestauranteListSucuralRestaurante);
            }
            List<Reservas> reservasList = restaurantes.getReservasList();
            for (Reservas reservasListReservas : reservasList) {
                reservasListReservas.setRestaurante(null);
                reservasListReservas = em.merge(reservasListReservas);
            }
            em.remove(restaurantes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Restaurantes> findRestaurantesEntities() {
        return findRestaurantesEntities(true, -1, -1);
    }

    public List<Restaurantes> findRestaurantesEntities(int maxResults, int firstResult) {
        return findRestaurantesEntities(false, maxResults, firstResult);
    }

    private List<Restaurantes> findRestaurantesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Restaurantes.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Restaurantes findRestaurantes(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Restaurantes.class, id);
        } finally {
            em.close();
        }
    }

    public int getRestaurantesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Restaurantes> rt = cq.from(Restaurantes.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
