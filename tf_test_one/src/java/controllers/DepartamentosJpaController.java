/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Municipios;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.Departamentos;
import persistencia.Direcciones;

/**
 *
 * @author Angel
 */
public class DepartamentosJpaController implements Serializable {

    public DepartamentosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Departamentos departamentos) {
        if (departamentos.getMunicipiosList() == null) {
            departamentos.setMunicipiosList(new ArrayList<Municipios>());
        }
        if (departamentos.getDireccionesList() == null) {
            departamentos.setDireccionesList(new ArrayList<Direcciones>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Municipios> attachedMunicipiosList = new ArrayList<Municipios>();
            for (Municipios municipiosListMunicipiosToAttach : departamentos.getMunicipiosList()) {
                municipiosListMunicipiosToAttach = em.getReference(municipiosListMunicipiosToAttach.getClass(), municipiosListMunicipiosToAttach.getIdMunicipio());
                attachedMunicipiosList.add(municipiosListMunicipiosToAttach);
            }
            departamentos.setMunicipiosList(attachedMunicipiosList);
            List<Direcciones> attachedDireccionesList = new ArrayList<Direcciones>();
            for (Direcciones direccionesListDireccionesToAttach : departamentos.getDireccionesList()) {
                direccionesListDireccionesToAttach = em.getReference(direccionesListDireccionesToAttach.getClass(), direccionesListDireccionesToAttach.getIdDireccion());
                attachedDireccionesList.add(direccionesListDireccionesToAttach);
            }
            departamentos.setDireccionesList(attachedDireccionesList);
            em.persist(departamentos);
            for (Municipios municipiosListMunicipios : departamentos.getMunicipiosList()) {
                Departamentos oldDepartamentoOfMunicipiosListMunicipios = municipiosListMunicipios.getDepartamento();
                municipiosListMunicipios.setDepartamento(departamentos);
                municipiosListMunicipios = em.merge(municipiosListMunicipios);
                if (oldDepartamentoOfMunicipiosListMunicipios != null) {
                    oldDepartamentoOfMunicipiosListMunicipios.getMunicipiosList().remove(municipiosListMunicipios);
                    oldDepartamentoOfMunicipiosListMunicipios = em.merge(oldDepartamentoOfMunicipiosListMunicipios);
                }
            }
            for (Direcciones direccionesListDirecciones : departamentos.getDireccionesList()) {
                Departamentos oldDepartamentoOfDireccionesListDirecciones = direccionesListDirecciones.getDepartamento();
                direccionesListDirecciones.setDepartamento(departamentos);
                direccionesListDirecciones = em.merge(direccionesListDirecciones);
                if (oldDepartamentoOfDireccionesListDirecciones != null) {
                    oldDepartamentoOfDireccionesListDirecciones.getDireccionesList().remove(direccionesListDirecciones);
                    oldDepartamentoOfDireccionesListDirecciones = em.merge(oldDepartamentoOfDireccionesListDirecciones);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Departamentos departamentos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamentos persistentDepartamentos = em.find(Departamentos.class, departamentos.getIdDepartamento());
            List<Municipios> municipiosListOld = persistentDepartamentos.getMunicipiosList();
            List<Municipios> municipiosListNew = departamentos.getMunicipiosList();
            List<Direcciones> direccionesListOld = persistentDepartamentos.getDireccionesList();
            List<Direcciones> direccionesListNew = departamentos.getDireccionesList();
            List<Municipios> attachedMunicipiosListNew = new ArrayList<Municipios>();
            for (Municipios municipiosListNewMunicipiosToAttach : municipiosListNew) {
                municipiosListNewMunicipiosToAttach = em.getReference(municipiosListNewMunicipiosToAttach.getClass(), municipiosListNewMunicipiosToAttach.getIdMunicipio());
                attachedMunicipiosListNew.add(municipiosListNewMunicipiosToAttach);
            }
            municipiosListNew = attachedMunicipiosListNew;
            departamentos.setMunicipiosList(municipiosListNew);
            List<Direcciones> attachedDireccionesListNew = new ArrayList<Direcciones>();
            for (Direcciones direccionesListNewDireccionesToAttach : direccionesListNew) {
                direccionesListNewDireccionesToAttach = em.getReference(direccionesListNewDireccionesToAttach.getClass(), direccionesListNewDireccionesToAttach.getIdDireccion());
                attachedDireccionesListNew.add(direccionesListNewDireccionesToAttach);
            }
            direccionesListNew = attachedDireccionesListNew;
            departamentos.setDireccionesList(direccionesListNew);
            departamentos = em.merge(departamentos);
            for (Municipios municipiosListOldMunicipios : municipiosListOld) {
                if (!municipiosListNew.contains(municipiosListOldMunicipios)) {
                    municipiosListOldMunicipios.setDepartamento(null);
                    municipiosListOldMunicipios = em.merge(municipiosListOldMunicipios);
                }
            }
            for (Municipios municipiosListNewMunicipios : municipiosListNew) {
                if (!municipiosListOld.contains(municipiosListNewMunicipios)) {
                    Departamentos oldDepartamentoOfMunicipiosListNewMunicipios = municipiosListNewMunicipios.getDepartamento();
                    municipiosListNewMunicipios.setDepartamento(departamentos);
                    municipiosListNewMunicipios = em.merge(municipiosListNewMunicipios);
                    if (oldDepartamentoOfMunicipiosListNewMunicipios != null && !oldDepartamentoOfMunicipiosListNewMunicipios.equals(departamentos)) {
                        oldDepartamentoOfMunicipiosListNewMunicipios.getMunicipiosList().remove(municipiosListNewMunicipios);
                        oldDepartamentoOfMunicipiosListNewMunicipios = em.merge(oldDepartamentoOfMunicipiosListNewMunicipios);
                    }
                }
            }
            for (Direcciones direccionesListOldDirecciones : direccionesListOld) {
                if (!direccionesListNew.contains(direccionesListOldDirecciones)) {
                    direccionesListOldDirecciones.setDepartamento(null);
                    direccionesListOldDirecciones = em.merge(direccionesListOldDirecciones);
                }
            }
            for (Direcciones direccionesListNewDirecciones : direccionesListNew) {
                if (!direccionesListOld.contains(direccionesListNewDirecciones)) {
                    Departamentos oldDepartamentoOfDireccionesListNewDirecciones = direccionesListNewDirecciones.getDepartamento();
                    direccionesListNewDirecciones.setDepartamento(departamentos);
                    direccionesListNewDirecciones = em.merge(direccionesListNewDirecciones);
                    if (oldDepartamentoOfDireccionesListNewDirecciones != null && !oldDepartamentoOfDireccionesListNewDirecciones.equals(departamentos)) {
                        oldDepartamentoOfDireccionesListNewDirecciones.getDireccionesList().remove(direccionesListNewDirecciones);
                        oldDepartamentoOfDireccionesListNewDirecciones = em.merge(oldDepartamentoOfDireccionesListNewDirecciones);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = departamentos.getIdDepartamento();
                if (findDepartamentos(id) == null) {
                    throw new NonexistentEntityException("The departamentos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Departamentos departamentos;
            try {
                departamentos = em.getReference(Departamentos.class, id);
                departamentos.getIdDepartamento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The departamentos with id " + id + " no longer exists.", enfe);
            }
            List<Municipios> municipiosList = departamentos.getMunicipiosList();
            for (Municipios municipiosListMunicipios : municipiosList) {
                municipiosListMunicipios.setDepartamento(null);
                municipiosListMunicipios = em.merge(municipiosListMunicipios);
            }
            List<Direcciones> direccionesList = departamentos.getDireccionesList();
            for (Direcciones direccionesListDirecciones : direccionesList) {
                direccionesListDirecciones.setDepartamento(null);
                direccionesListDirecciones = em.merge(direccionesListDirecciones);
            }
            em.remove(departamentos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Departamentos> findDepartamentosEntities() {
        return findDepartamentosEntities(true, -1, -1);
    }

    public List<Departamentos> findDepartamentosEntities(int maxResults, int firstResult) {
        return findDepartamentosEntities(false, maxResults, firstResult);
    }

    private List<Departamentos> findDepartamentosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Departamentos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Departamentos findDepartamentos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Departamentos.class, id);
        } finally {
            em.close();
        }
    }

    public int getDepartamentosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Departamentos> rt = cq.from(Departamentos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
