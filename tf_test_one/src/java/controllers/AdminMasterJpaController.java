/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.AdminMaster;

/**
 *
 * @author Angel
 */
public class AdminMasterJpaController implements Serializable {

    public AdminMasterJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AdminMaster adminMaster) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(adminMaster);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AdminMaster adminMaster) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            adminMaster = em.merge(adminMaster);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = adminMaster.getIdMaster();
                if (findAdminMaster(id) == null) {
                    throw new NonexistentEntityException("The adminMaster with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AdminMaster adminMaster;
            try {
                adminMaster = em.getReference(AdminMaster.class, id);
                adminMaster.getIdMaster();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The adminMaster with id " + id + " no longer exists.", enfe);
            }
            em.remove(adminMaster);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AdminMaster> findAdminMasterEntities() {
        return findAdminMasterEntities(true, -1, -1);
    }

    public List<AdminMaster> findAdminMasterEntities(int maxResults, int firstResult) {
        return findAdminMasterEntities(false, maxResults, firstResult);
    }

    private List<AdminMaster> findAdminMasterEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AdminMaster.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AdminMaster findAdminMaster(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AdminMaster.class, id);
        } finally {
            em.close();
        }
    }

    public int getAdminMasterCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AdminMaster> rt = cq.from(AdminMaster.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
