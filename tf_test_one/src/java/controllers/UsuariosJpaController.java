/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.TipoUser;
import persistencia.Reservas;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import persistencia.Usuarios;

/**
 *
 * @author Angel
 */
public class UsuariosJpaController implements Serializable {

    public UsuariosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuarios usuarios) {
        if (usuarios.getReservasList() == null) {
            usuarios.setReservasList(new ArrayList<Reservas>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TipoUser tipoUser = usuarios.getTipoUser();
            if (tipoUser != null) {
                tipoUser = em.getReference(tipoUser.getClass(), tipoUser.getIdTipoUser());
                usuarios.setTipoUser(tipoUser);
            }
            List<Reservas> attachedReservasList = new ArrayList<Reservas>();
            for (Reservas reservasListReservasToAttach : usuarios.getReservasList()) {
                reservasListReservasToAttach = em.getReference(reservasListReservasToAttach.getClass(), reservasListReservasToAttach.getIdReserva());
                attachedReservasList.add(reservasListReservasToAttach);
            }
            usuarios.setReservasList(attachedReservasList);
            em.persist(usuarios);
            if (tipoUser != null) {
                tipoUser.getUsuariosList().add(usuarios);
                tipoUser = em.merge(tipoUser);
            }
            for (Reservas reservasListReservas : usuarios.getReservasList()) {
                Usuarios oldNombreclienteOfReservasListReservas = reservasListReservas.getNombrecliente();
                reservasListReservas.setNombrecliente(usuarios);
                reservasListReservas = em.merge(reservasListReservas);
                if (oldNombreclienteOfReservasListReservas != null) {
                    oldNombreclienteOfReservasListReservas.getReservasList().remove(reservasListReservas);
                    oldNombreclienteOfReservasListReservas = em.merge(oldNombreclienteOfReservasListReservas);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuarios usuarios) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuarios persistentUsuarios = em.find(Usuarios.class, usuarios.getIdUsuario());
            TipoUser tipoUserOld = persistentUsuarios.getTipoUser();
            TipoUser tipoUserNew = usuarios.getTipoUser();
            List<Reservas> reservasListOld = persistentUsuarios.getReservasList();
            List<Reservas> reservasListNew = usuarios.getReservasList();
            if (tipoUserNew != null) {
                tipoUserNew = em.getReference(tipoUserNew.getClass(), tipoUserNew.getIdTipoUser());
                usuarios.setTipoUser(tipoUserNew);
            }
            List<Reservas> attachedReservasListNew = new ArrayList<Reservas>();
            for (Reservas reservasListNewReservasToAttach : reservasListNew) {
                reservasListNewReservasToAttach = em.getReference(reservasListNewReservasToAttach.getClass(), reservasListNewReservasToAttach.getIdReserva());
                attachedReservasListNew.add(reservasListNewReservasToAttach);
            }
            reservasListNew = attachedReservasListNew;
            usuarios.setReservasList(reservasListNew);
            usuarios = em.merge(usuarios);
            if (tipoUserOld != null && !tipoUserOld.equals(tipoUserNew)) {
                tipoUserOld.getUsuariosList().remove(usuarios);
                tipoUserOld = em.merge(tipoUserOld);
            }
            if (tipoUserNew != null && !tipoUserNew.equals(tipoUserOld)) {
                tipoUserNew.getUsuariosList().add(usuarios);
                tipoUserNew = em.merge(tipoUserNew);
            }
            for (Reservas reservasListOldReservas : reservasListOld) {
                if (!reservasListNew.contains(reservasListOldReservas)) {
                    reservasListOldReservas.setNombrecliente(null);
                    reservasListOldReservas = em.merge(reservasListOldReservas);
                }
            }
            for (Reservas reservasListNewReservas : reservasListNew) {
                if (!reservasListOld.contains(reservasListNewReservas)) {
                    Usuarios oldNombreclienteOfReservasListNewReservas = reservasListNewReservas.getNombrecliente();
                    reservasListNewReservas.setNombrecliente(usuarios);
                    reservasListNewReservas = em.merge(reservasListNewReservas);
                    if (oldNombreclienteOfReservasListNewReservas != null && !oldNombreclienteOfReservasListNewReservas.equals(usuarios)) {
                        oldNombreclienteOfReservasListNewReservas.getReservasList().remove(reservasListNewReservas);
                        oldNombreclienteOfReservasListNewReservas = em.merge(oldNombreclienteOfReservasListNewReservas);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = usuarios.getIdUsuario();
                if (findUsuarios(id) == null) {
                    throw new NonexistentEntityException("The usuarios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuarios usuarios;
            try {
                usuarios = em.getReference(Usuarios.class, id);
                usuarios.getIdUsuario();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuarios with id " + id + " no longer exists.", enfe);
            }
            TipoUser tipoUser = usuarios.getTipoUser();
            if (tipoUser != null) {
                tipoUser.getUsuariosList().remove(usuarios);
                tipoUser = em.merge(tipoUser);
            }
            List<Reservas> reservasList = usuarios.getReservasList();
            for (Reservas reservasListReservas : reservasList) {
                reservasListReservas.setNombrecliente(null);
                reservasListReservas = em.merge(reservasListReservas);
            }
            em.remove(usuarios);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuarios> findUsuariosEntities() {
        return findUsuariosEntities(true, -1, -1);
    }

    public List<Usuarios> findUsuariosEntities(int maxResults, int firstResult) {
        return findUsuariosEntities(false, maxResults, firstResult);
    }

    private List<Usuarios> findUsuariosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuarios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuarios findUsuarios(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuarios.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuariosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuarios> rt = cq.from(Usuarios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
