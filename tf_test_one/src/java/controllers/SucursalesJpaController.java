/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import persistencia.Municipios;
import persistencia.SucuralRestaurante;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import persistencia.Sucursales;

/**
 *
 * @author Angel
 */
public class SucursalesJpaController implements Serializable {

    private EntityManagerFactory emf = null;

    public SucursalesJpaController() {
        this.emf = Persistence.createEntityManagerFactory("tf_test_onePU");
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    //METODO VER LAS SUCURSALES SEGUN EL RESTAURANTE
    public List ver_sucursales_cheff(){
        List<SucuralRestaurante> sucursales = null;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {                           //select detalles from sucursales where sucursal = 'El Chef 87';
            Query query = em.createQuery("SELECT s FROM Sucursales s WHERE s.sucursal = 'El Chef 87'");
            em.getTransaction().commit();
            sucursales = query.getResultList();
        } catch (Exception e) {
            System.err.println("ERROR: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return sucursales;
    }
    //METODO VER LAS SUCURSALES SEGUN EL RESTAURANTE
    public List ver_sucursales_PANDA(String sucursal){
        List<SucuralRestaurante> sucursales = null;
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {                           //select detalles from sucursales where sucursal = 'El Chef 87';
            Query query = em.createQuery("SELECT s FROM Sucursales s WHERE s.sucursal= :suc");
            query.setParameter("suc", sucursal);
            em.getTransaction().commit();
            sucursales = query.getResultList();
        } catch (Exception e) {
            System.err.println("ERROR: " + e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return sucursales;
    }

    public void create(Sucursales sucursales) {
        if (sucursales.getSucuralRestauranteList() == null) {
            sucursales.setSucuralRestauranteList(new ArrayList<SucuralRestaurante>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Municipios municipio = sucursales.getMunicipio();
            if (municipio != null) {
                municipio = em.getReference(municipio.getClass(), municipio.getIdMunicipio());
                sucursales.setMunicipio(municipio);
            }
            List<SucuralRestaurante> attachedSucuralRestauranteList = new ArrayList<SucuralRestaurante>();
            for (SucuralRestaurante sucuralRestauranteListSucuralRestauranteToAttach : sucursales.getSucuralRestauranteList()) {
                sucuralRestauranteListSucuralRestauranteToAttach = em.getReference(sucuralRestauranteListSucuralRestauranteToAttach.getClass(), sucuralRestauranteListSucuralRestauranteToAttach.getIdPivoSucursalRestaurante());
                attachedSucuralRestauranteList.add(sucuralRestauranteListSucuralRestauranteToAttach);
            }
            sucursales.setSucuralRestauranteList(attachedSucuralRestauranteList);
            em.persist(sucursales);
            if (municipio != null) {
                municipio.getSucursalesList().add(sucursales);
                municipio = em.merge(municipio);
            }
            for (SucuralRestaurante sucuralRestauranteListSucuralRestaurante : sucursales.getSucuralRestauranteList()) {
                Sucursales oldIdSucursalOfSucuralRestauranteListSucuralRestaurante = sucuralRestauranteListSucuralRestaurante.getIdSucursal();
                sucuralRestauranteListSucuralRestaurante.setIdSucursal(sucursales);
                sucuralRestauranteListSucuralRestaurante = em.merge(sucuralRestauranteListSucuralRestaurante);
                if (oldIdSucursalOfSucuralRestauranteListSucuralRestaurante != null) {
                    oldIdSucursalOfSucuralRestauranteListSucuralRestaurante.getSucuralRestauranteList().remove(sucuralRestauranteListSucuralRestaurante);
                    oldIdSucursalOfSucuralRestauranteListSucuralRestaurante = em.merge(oldIdSucursalOfSucuralRestauranteListSucuralRestaurante);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sucursales sucursales) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursales persistentSucursales = em.find(Sucursales.class, sucursales.getIdSucursal());
            Municipios municipioOld = persistentSucursales.getMunicipio();
            Municipios municipioNew = sucursales.getMunicipio();
            List<SucuralRestaurante> sucuralRestauranteListOld = persistentSucursales.getSucuralRestauranteList();
            List<SucuralRestaurante> sucuralRestauranteListNew = sucursales.getSucuralRestauranteList();
            if (municipioNew != null) {
                municipioNew = em.getReference(municipioNew.getClass(), municipioNew.getIdMunicipio());
                sucursales.setMunicipio(municipioNew);
            }
            List<SucuralRestaurante> attachedSucuralRestauranteListNew = new ArrayList<SucuralRestaurante>();
            for (SucuralRestaurante sucuralRestauranteListNewSucuralRestauranteToAttach : sucuralRestauranteListNew) {
                sucuralRestauranteListNewSucuralRestauranteToAttach = em.getReference(sucuralRestauranteListNewSucuralRestauranteToAttach.getClass(), sucuralRestauranteListNewSucuralRestauranteToAttach.getIdPivoSucursalRestaurante());
                attachedSucuralRestauranteListNew.add(sucuralRestauranteListNewSucuralRestauranteToAttach);
            }
            sucuralRestauranteListNew = attachedSucuralRestauranteListNew;
            sucursales.setSucuralRestauranteList(sucuralRestauranteListNew);
            sucursales = em.merge(sucursales);
            if (municipioOld != null && !municipioOld.equals(municipioNew)) {
                municipioOld.getSucursalesList().remove(sucursales);
                municipioOld = em.merge(municipioOld);
            }
            if (municipioNew != null && !municipioNew.equals(municipioOld)) {
                municipioNew.getSucursalesList().add(sucursales);
                municipioNew = em.merge(municipioNew);
            }
            for (SucuralRestaurante sucuralRestauranteListOldSucuralRestaurante : sucuralRestauranteListOld) {
                if (!sucuralRestauranteListNew.contains(sucuralRestauranteListOldSucuralRestaurante)) {
                    sucuralRestauranteListOldSucuralRestaurante.setIdSucursal(null);
                    sucuralRestauranteListOldSucuralRestaurante = em.merge(sucuralRestauranteListOldSucuralRestaurante);
                }
            }
            for (SucuralRestaurante sucuralRestauranteListNewSucuralRestaurante : sucuralRestauranteListNew) {
                if (!sucuralRestauranteListOld.contains(sucuralRestauranteListNewSucuralRestaurante)) {
                    Sucursales oldIdSucursalOfSucuralRestauranteListNewSucuralRestaurante = sucuralRestauranteListNewSucuralRestaurante.getIdSucursal();
                    sucuralRestauranteListNewSucuralRestaurante.setIdSucursal(sucursales);
                    sucuralRestauranteListNewSucuralRestaurante = em.merge(sucuralRestauranteListNewSucuralRestaurante);
                    if (oldIdSucursalOfSucuralRestauranteListNewSucuralRestaurante != null && !oldIdSucursalOfSucuralRestauranteListNewSucuralRestaurante.equals(sucursales)) {
                        oldIdSucursalOfSucuralRestauranteListNewSucuralRestaurante.getSucuralRestauranteList().remove(sucuralRestauranteListNewSucuralRestaurante);
                        oldIdSucursalOfSucuralRestauranteListNewSucuralRestaurante = em.merge(oldIdSucursalOfSucuralRestauranteListNewSucuralRestaurante);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sucursales.getIdSucursal();
                if (findSucursales(id) == null) {
                    throw new NonexistentEntityException("The sucursales with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sucursales sucursales;
            try {
                sucursales = em.getReference(Sucursales.class, id);
                sucursales.getIdSucursal();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sucursales with id " + id + " no longer exists.", enfe);
            }
            Municipios municipio = sucursales.getMunicipio();
            if (municipio != null) {
                municipio.getSucursalesList().remove(sucursales);
                municipio = em.merge(municipio);
            }
            List<SucuralRestaurante> sucuralRestauranteList = sucursales.getSucuralRestauranteList();
            for (SucuralRestaurante sucuralRestauranteListSucuralRestaurante : sucuralRestauranteList) {
                sucuralRestauranteListSucuralRestaurante.setIdSucursal(null);
                sucuralRestauranteListSucuralRestaurante = em.merge(sucuralRestauranteListSucuralRestaurante);
            }
            em.remove(sucursales);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sucursales> findSucursalesEntities() {
        return findSucursalesEntities(true, -1, -1);
    }

    public List<Sucursales> findSucursalesEntities(int maxResults, int firstResult) {
        return findSucursalesEntities(false, maxResults, firstResult);
    }

    private List<Sucursales> findSucursalesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sucursales.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sucursales findSucursales(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sucursales.class, id);
        } finally {
            em.close();
        }
    }

    public int getSucursalesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sucursales> rt = cq.from(Sucursales.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
