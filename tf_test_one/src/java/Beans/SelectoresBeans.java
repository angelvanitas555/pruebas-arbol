/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import controllers.RestaurantesJpaController;
import controllers.SucuralRestauranteJpaController;
import controllers.SucursalesJpaController;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.AjaxBehaviorEvent;
import persistencia.Restaurantes;
import persistencia.SucuralRestaurante;
import persistencia.Sucursales;

/**
 *
 * @author Angel
 */
@ManagedBean(name = "restau")//comentario de cambia
@RequestScoped
public class SelectoresBeans {

    /* La idea es que del pivote de surcursal_restarurante poder llenar selectores,
    si el selector de panda expores está seleccionado debe de llenar otro selector
    con las sucursales de panda expres que tiene por departamento y otro para llenar
    un selector con sus respectivas sucursales    
     */
    private String names;

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    private Restaurantes comedores = new Restaurantes();
    private RestaurantesJpaController comedores_control = new RestaurantesJpaController();

    private SucuralRestaurante pivote = new SucuralRestaurante();
    private SucuralRestauranteJpaController control = new SucuralRestauranteJpaController();

    private Sucursales salas = new Sucursales();
    private SucursalesJpaController salas_control = new SucursalesJpaController();

    private Integer id_helper_pivote;
    private Integer id_helper_sala;
    private Integer id_helper_comedor;

    public SucuralRestaurante getPivote() {
        return pivote;
    }

    public Sucursales getSalas() {
        return salas;
    }

    public void setSalas(Sucursales salas) {
        this.salas = salas;
    }

    public SucursalesJpaController getSalas_control() {
        return salas_control;
    }

    public void setSalas_control(SucursalesJpaController salas_control) {
        this.salas_control = salas_control;
    }

    public Integer getId_helper_sala() {
        return id_helper_sala;
    }

    public void setId_helper_sala(Integer id_helper_sala) {
        this.id_helper_sala = id_helper_sala;
    }

    public void setPivote(SucuralRestaurante pivote) {
        this.pivote = pivote;
    }

    public SucuralRestauranteJpaController getControl() {
        return control;
    }

    public void setControl(SucuralRestauranteJpaController control) {
        this.control = control;
    }

    public Integer getId_helper_pivote() {
        return id_helper_pivote;
    }

    public void setId_helper_pivote(Integer id_helper_pivote) {
        this.id_helper_pivote = id_helper_pivote;
    }

    public Restaurantes getComedores() {
        return comedores;
    }

    public void setComedores(Restaurantes comedores) {
        this.comedores = comedores;
    }

    public RestaurantesJpaController getComedores_control() {
        return comedores_control;
    }

    public void setComedores_control(RestaurantesJpaController comedores_control) {
        this.comedores_control = comedores_control;
    }

    public Integer getId_helper_comedor() {
        return id_helper_comedor;
    }

    public void setId_helper_comedor(Integer id_helper_comedor) {
        this.id_helper_comedor = id_helper_comedor;
    }

    public SelectoresBeans() {
    }

    public void selctores_x_restaurantes() {

    }
    
    public List listarestaurantes() {
       return salas_control.ver_sucursales_PANDA(names); 
    }

    // cargar el selector de sucursales segun el restaurante elegido
    public void cargar_secursales(AjaxBehaviorEvent event) {
        salas_control.findSucursales(0).toString();
        String nombre = salas.getSucursal();
        id_helper_comedor = comedores.getIdRestaurante();

        if (nombre.equals("El Chef 87")) {
            salas.getSucursal().equals("El Chef 87");
        }
    }

}
